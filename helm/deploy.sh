#!/bin/bash

kubectl apply -f templates/app-deployment.yaml
kubectl apply -f templates/service.yaml
kubectl apply -f templates/ingress.yaml
